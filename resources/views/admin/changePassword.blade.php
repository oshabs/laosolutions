@extends('admin.layout')
@section("content")
<div class="project-upload-wrapper">
    <div class="container">
        @if(Session::has("msg"))
            <div class="alert alert-success">{{Session::get('msg')}}</div>
        @endif
        <div class="card">
            <div class="card-body">
        <h2>Change Password</h2>
        <hr>

        <form action="{{url('admin/change-password')}}" method="post"  enctype="multipart/form-data">
        @csrf
                    <div class="form-group"><label for="title">Old Password</label><input type="password" name="oldPassword" id="" class="form-control">
                    <span>@if($errors->has('title')) {{ $errors->first('title')}} @endif</span>
                </div>
                    <div class="form-group"><label for="title">New Password</label><input type="password" name="newPassword" id="" class="form-control">
                    <span>@if($errors->has('title')) {{ $errors->first('title')}} @endif</span>
                </div>
                    <div class="form-group"><label for="title">Confirm Password</label><input type="password" name="cPassword" id="" class="form-control">
                    <span>@if($errors->has('title')) {{ $errors->first('title')}} @endif</span>
                </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
            </div>
        </div>
    </div>
</div>
@endsection