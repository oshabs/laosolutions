@extends('admin.layout')
@section("content")
<div class="project-upload-wrapper">
    <div class="container">
        @if(Session::has("msg"))
            <div class="alert alert-danger">{{Session::get('msg')}}</div>
        @endif
        <div class="card">
            <div class="card-body">
        <h2>All Project</h2>
        <hr>
        <table class="table  table-hover">
  <thead>
    <tr>
      <th scope="col"  width="10%">#</th>
      <th scope="col" width="50%">Title</th>
      <th scope="col" width="20%">Date Uploaded</th>
      <th scope="col">Action
      </th>
    </tr>
  </thead>
  <tbody>
      <?php $i = 1;?>
      @foreach($projects as $project)
      <tr>
          <td>{{$i++}}</td>
          <td>{{$project->title}}</td>
          <td>{{Carbon\Carbon::parse($project->created_at)->format("d M, Y")}}</td>
          <td>
              <a href="{{url('admin/view/project/'. $project->id)}}">
          <span class="material-icons">
            remove_red_eye
            </span>

              </a>
              <a href="{{url('admin/project/edit/'. $project->id)}}">
                <span class="material-icons">
                edit
                </span>
              </a>
              <a href="#"   data-toggle="modal" data-target="#exampleModal{{$project->id}}">
                <span class="material-icons" style="color: red">
                    delete
                </span>
              </a>
              
            <!-- Modal -->
            <div class="modal fade" id="exampleModal{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                    <span class="material-icons">
warning
</span>
Alert
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this project?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <a href="{{url('admin/project/delete/'. $project->id)}}" class="btn btn-danger">Yes</a>
                </div>
                </div>
            </div>
            </div>
          </td>
      </tr>
      @endforeach
  </tbody>
            </div>
        </div>
    </div>
</div>
@endsection