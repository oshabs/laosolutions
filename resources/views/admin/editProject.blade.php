@extends('admin.layout')
@section("content")
<div class="project-upload-wrapper">
    <div class="container">
        @if(Session::has("msg"))
            <div class="alert alert-success">{{Session::get('msg')}}</div>
        @endif
        <div class="card">
            <div class="card-body">
        <h2>Edit Project</h2>
        <hr>
        @foreach($projects as $project)

        <form action="{{url('admin/edit/'.$project->id )}}" method="post"  enctype="multipart/form-data">
        @csrf
                    <div class="form-group"><label for="title">Title</label><input type="text" value="{{$project->title}}" name="title" id="" class="form-control">
                    <span>@if($errors->has('title')) {{ $errors->first('title')}} @endif</span>
                </div>
            <div class="form-group"><label for="description">Description of the Project</label>
                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$project->description}}</textarea>
                <span>@if($errors->has('description')) {{ $errors->first('description')}} @endif</span>
                
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>
        </form>
        @endforeach
        
            </div>
        </div>
    </div>
</div>
@endsection