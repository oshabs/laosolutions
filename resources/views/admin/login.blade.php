<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    
    <title>Login</title>
</head>
<body>
    <style>
    body{
        margin: 0px;
        padding: 0px;

    }
    .photo {
        
        object-fit: cover;
        height: 100vh;
        max-width: 100%;
    }
    .row{
        margin: 0px;
        padding: 0px;
    }
    .card{
        margin-top: 20em;
    }
    span{
        color: red;
    }
    </style>
    <div class="wrapper">
        <div class="row">
            <div class="col-md-5 photo">
                <img src="{{asset('login.jpg')}}" class='photo' alt="">
            </div>
            <div class="col-md-5 ">
                <div class=" d-flex justify-content-center">
                <div class="card" style="min-width: 70%;">
                    <div class="card-body">
                        <h2>Login</h2>
                        <form action="{{url('admin/check-login-details')}}" method="post">
                            @csrf
                        <div class="form-group"><label for="username">Username</label><input type="text" name="username" id="username" class="form-control">
                        <span>@if($errors->has('username')) {{ $errors->first('username')}} @endif</span>
                        
                        </div>
                                               
                        <div class="form-group"><label for="password">Password</label><input type="password" name="password" id="password" class="form-control">
                        <span>@if($errors->has('password')) {{ $errors->first('password')}} @endif</span>
                        
                        </div>
                        <div class="form-group"> 
                        <button type="submit" class="btn primary-color">Login</button>

                        </div>
                        </form>
                    </div>
                </div>
    
                </div>
            </div>
        </div>
    </div>
</body> 
</html>