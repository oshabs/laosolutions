<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="{{asset('admin_style.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
   
</head>
<body>
<nav class="navbar navbar-expand-lg customize-navbar">
  <div class="container">
<a class="navbar-brand" href="#">
            <img src="{{asset("logo.jpg")}}" class="img-fluid" style="width: 150px; height: 50px; object-fit:cover;" alt="">
                
            </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{url('admin/dashboard')}}">Dashboard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('admin/all-project')}}">All Project</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('admin/upload-project')}}">Upload Project</a>
      </li>
    </ul>
    <ul class="navbar-nav">
        <li style="margin: .5rem;">Welcome, Admin</li>
        
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="material-icons">
account_circle
</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{url('admin/change-password')}}">Change Password</a>
          <a class="dropdown-item" href="{{url('admin/sign-out')}}">Sign out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

  </div>
@yield("content")
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>

</body>
</html>