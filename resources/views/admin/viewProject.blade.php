@extends('admin.layout')
@section("content")
<div class="project-upload-wrapper" style="margin-bottom: 10rem;">
    <div class="container">
        @if(Session::has("msg"))
            <div class="alert alert-success">{{Session::get('msg')}}</div>
        @endif
        @foreach($projects as $project)   
        
        <div class="card">
            <div class="card-body">
        <h2>{{$project->title}}</h2>
        <hr>
        <img src="{{asset('/projects/'. $project->photo_path)}}" alt="" class="img-fluid">
        <h2 class="mt-5">Description</h2>
        <hr>
        <p>
            {{$project->description}}
        </p>
        @endforeach
    </div>
</div>
@endsection