@extends('admin.layout')
@section("content")
<div class="dashboard-wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <span class="nofproject">{{\App\Projects::count()}}</span>
                    <span class="headline">Total Projects Uploaded</span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <span class="nofproject">{{\App\Projects::whereDate('created_at', '<=', \Carbon\Carbon::now())->count()}}</span>
                    <span class="headline">Total Projects Uploaded Today</span>
                </div>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>

</div>
@endsection