@extends("site.layout")
@section("content")
<style>
    h1{
        padding-bottom: 2rem;
    }
</style>
<div class="all-project-wrapper">
<div class="container">
    <h1>
        <div class="row">
            <span class="material-icons">
            miscellaneous_services
            </span>
            All Projects
        </div>
    </h1>

<div class="row">
            <div class="col-md-4 first">
                <img src="{{asset("img1.png")}}" clas="img-fluid" alt="">
            </div>
            <div class="col-md-4 second">
                <img src="{{asset("img2.jpg")}}"  clas="img-fluid" alt="">
            </div>
            <div class="col-md-4 third">
                <img src="{{asset("img3.png")}}"  clas="img-fluid" alt="">
            </div>
        </div>
</div>
@endsection