<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="{{asset('flip.css')}}"> -->
    <link rel="stylesheet" href="{{asset('bootstrapcarousel.css')}}">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('animate.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.1.0/css/hover-min.css">
      <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css"> -->
    <title>Home</title>
</head>
<body>
    <div class="header-content">

    <div class="header-section">
        <div class="container">
            <div class="right-section">
            <span> customercare@gmail.com</span>
            <span> 24/7 Support</span>
            <span> Support: +234 703 474 8722; +234 802 399 3865</span>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg customize-navbar">
        <div class="container">
            <a class="navbar-brand" href="#">
            <img src="{{asset("logo.jpg")}}" class="img-fluid" style="width: 150px; height: 50px; object-fit:cover;" alt="">
                
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="material-icons">
menu
</span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/services')}}">Services</a>
                </li>
                </li>   
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/contact-us')}}">Contact us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/all-projects')}}">All Projects</a>
                </li>
                </ul>
            </div>    
        </div>
    </nav>
    </div>
        
    <div class="wrapper">
        @yield("content")
    </div>
    
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="content">
                    <h5>LAO SOLUTIONS</h5>
                    <p style="width: 20rem;">
                        Our company builds customized software applications for clients and to make sure all our apps work online realtime.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="content">
                    <h5>Menu</h5>
                    <ul>
                        <li><a href="">Home</a> </li>
                        <li> <a href="{{url('/about-us')}}">About Us</a> </li>
                        <li> <a href="{{url('/services')}}">Services</a> </li>
                        <li><a href="{{url('/contact-us')}}">Contact Us</a> </li>
                        <li><a href="{{url('/all-projects')}}">All Projects</a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="content">
                    <h5>What We Offer</h5>
                    <ul>
                    <li>Geographic Information Systems</li>
                    <li>Custom Software Development</li>
                    <li>Web/Application Development </li>
                    <li>Quality Assurance</li>
                    <li>Web based Community Solutions.</li>
                    <li>Client Server Applications</li>
                    <li>Enterprise Application Integration</li>
                    
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="content contact">
                <h5>Our Contacts</h5>
                    <ul>
                        <li>
                            <span class="material-icons">
                                phone
                            </span>
                            +234 703 474 8722; +234 802 399 3865
                        </li>
                        <li>
                            <span class="material-icons">
                            location_on
                            </span> 13 Oke Street gbagada

                        </li>
                        <li>
                        <span class="material-icons">
                            email
                        </span>
                        customercare@laosolutions.com
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
    <div class="copy-right">
        <div class="container">
        &copy;Copyright LAO Solutions 2020.
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <!-- <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> -->
    <script src="{{asset('custom.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script>$('.carousel').carousel(); AOS.init(); </script>
    
</body>
</html>