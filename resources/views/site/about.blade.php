@extends("site.layout")
@section("content")

<div class="about-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
    <h1>
        <div class="row">
    <span class="material-icons">
pages
</span> <span>ABOUT US</span> 

        </div>
                    </h1>
                    <p>
                    LAO Solutions Nigerian based company managed by highly experienced information technology professionals and offers consulting services in the areas of Software Development, Government to Citizen Solutions, Business Development, IT Infrastructure Management, Quality Assurance and implementation. LAO Solutions has considerable experience delivering solutions in single and multi operating system environments.

                    </p>
                    <p>
                    LAO Solutions through its alliances is able to attain goals and objectives which one company cannot achieve alone. These are based on a win-win mindset, mutual trust, commitment and the sharing of risks and rewards to benefit customers directly. Our professionals have several hundred person-years of experience consulting with organizations and implementing our proposed and customer desired solutions. 
                    </p>
    </div>
            <div class="col-md-6">
            <img src="{{asset("peach/scene/scene1.svg")}}" class="img-fluid" alt="">                
                
            </div>
        </div>
</div>
        
</div>
@endsection