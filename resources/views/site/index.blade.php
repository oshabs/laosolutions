@extends("site.layout")
@section("content")
<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="{{asset('firstslide.jpg')}}">
    <div class="carousel-caption first-carousel-caption animate__animated animate__rotateInDownRight" >
        <h1>We are Reliable</h1>
        <h3>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, consequatur.</h3>
    </div>
    </div>
    <div class="carousel-item">
    <img src="{{asset('secondslide.jpg')}}">
    <div class="carousel-caption second-carousel-caption animate__animated animate__slideInDown">
        <h1 style="color: #000;">We are Reliable</h1>
        <h3 style="color: #000;">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, consequatur.</h3>
    </div>
    </div>
    <div class="carousel-item">
    <img src="{{asset('thirdslide.jpg')}}">
    <div class="carousel-caption third-carousel-caption animate__animated animate__slideInDown">
        <h1>We are Reliable</h1>
        <h3>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, consequatur.</h3>
    </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<div class="mission"

>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="content">
                <h2>
                    OUR MISSION
                </h2>
                <p>
                To provide highest quality services and solutions to our clients and a productive and innovative environment for our employees to excel in the world of Information Technology.
                </p>

                </div>

            </div>

            <div class="col-md-5">
                <img src="{{asset("peach/scene/scene2.svg")}}" class="img-fluid" alt="">
            </div>
        </div>
        
    </div>
</div>
<section class="our-services"

>
    <h1>Our Services</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4" >
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
                            location_on
                            </span>
                    </div>
                    <h4>
                        Geographic Information Systems
                    </h4>
                   
                </div>
            </div>
            <div class="col-md-4"
            >
                <div class="service-content">
                    <div class="contain-icon">
                    <i class="fas fa-stream"></i>
                    </div>
                    <h4>
                        Custom Software Development
                    </h4>
                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-content">
                    <div class="contain-icon">
                    
                    <span class="material-icons">
cloud
</span>
                    </div>
                    <h4>
                        Web/Application Development 
                    </h4>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<div class="d-flex justify-content-center">
<a href="{{url('/services')}}" class="btn primary-color service" style="margin-bottom: 7em;">See All Services >>></a>

</div>
<section class="why-choose-us" 

>
<h2>Why Choose Us</h2>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                High Quality Hardware
                </h4>
                <p>
                We use top-notch hardware to develop the most efficient apps for our customers
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                Dedicated 24\7 Support
                </h4>
                <p>
                You can rely on our 24/7 tech support that will gladly solve any app issue you may have.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                Design And Build
                </h4>
                <p>
                    We use UI apps to design our software to customer's satisfaction before building
                </p>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 3rem;">
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                
                    Agile and Fast Working Style
                </h4>
                <p>
                     We workwith seasoned professionals in every sector in developing apps related to that sector. 
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                Some Apps are Free
                </h4>
                <p>
                    We also develop free apps that can be downloaded online without any payments.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="why-container">
                <h4>
                High Level of Usability
                </h4>
                <p>
                All our products have high usability allowing users to easily operate the apps.
                </p>
            </div>
        </div>
    </div>
</div>
</section>
<!-- <section class="experience"
data-aos-once="true"

    data-aos="zoom-out"
    data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-easing="ease-in-out"
    data-aos-mirror="true"
    data-aos-once="false"
    data-aos-anchor-placement="top-center"
>
    
    <div class="card" style="width: 65rem;">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h1>50</h1>
                    <p class="years">
                        Years of Experience
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 3rem;  border-right: 0rem;">
                    <p class="rend">20</p>
                    <p class="cont">Web Apps Developed</p>
                    <p class="rend">20</p>
                    <p class="cont">Mobile Apps Developed</p>
                    <p class="rend">5</p>
                    <p class="cont">Employees</p>
                </div>
            </div>
        </div>
    

    </div>
</section> -->
<div class="about">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-7">
                    <div class="about-us">
                <h2>
                        ABOUT US
                    </h2>
                    <p>
                    LAO Solutions Nigerian based company managed by highly experienced information technology professionals and offers consulting services in the areas of Software Development, Government to Citizen Solutions, Business Development, IT Infrastructure Management, Quality Assurance and implementation. LAO Solutions has considerable experience delivering solutions in single and multi operating system environments.

                    </p>
                    <a href="{{url('about-us')}}" class="btn primary-color">See More</a>

                    </div>
                </div>
                <div class="col-md-5">
                <img src="{{asset("peach/scene/scene1.svg")}}" class="img-fluid" alt="">                
                </div>
            </div>

        </div>
    </div>
</div>
<!-- <div class="our-clients">
    <h2>Our Clients</h2>
    <div class="row">
        <div class="col-md-4">
            SEO CONCEPTS
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
</div> -->
<section class="latest-project"
>
<h2>Latest Project</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4 first">
                <img src="{{asset("img1.png")}}" clas="img-fluid" alt="">
            </div>
            <div class="col-md-4 second">
                <img src="{{asset("img2.jpg")}}"  clas="img-fluid" alt="">
            </div>
            <div class="col-md-4 third">
                <img src="{{asset("img3.png")}}"  clas="img-fluid" alt="">
            </div>
        </div>
    </div>
    <div class="project-link d-flex justify-content-center">
        <a href="{{url('/all-projects')}}" class="btn primary-color hvr-bounce-to-right">All Project >>></a>
    </div>
</section>
@endsection