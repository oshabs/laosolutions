@extends("site.layout")
@section("content")
    <div class="contact-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2><span class="material-icons">
contact_page
</span>CONTACT INFORMATION</h2>
                    <ul>
                        <li>
                            <span class="material-icons">
                                phone
                            </span>
                            
                            +234 703 474 8722; +234 802 399 3865
                        </li>
                        <li>
                            <span class="material-icons">
                            location_on
                            </span> 13 Oke Street gbagada

                        </li>
                        <li>
                        <span class="material-icons">
                            email
                        </span>
                        customercare@laosolutions.com
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="">First Name</label><input type="text" placeholder="First Name" name="first_name" id="" class="form-control"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="">Last Name</label><input type="text" placeholder="Last Name" name="last_name" id="" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group"
                                        ><label for="">Message</label><textarea name="message" class="form-control" placeholder="message" id="" cols="30" rows="10">

                                        </textarea>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn primary-color w-block">Submit</button>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection