@extends("site.layout")
@section("content")
<style>
    .row{
        margin-bottom: 4rem;
    }
</style>
    <div class="service-wrapper">
        
<section class="our-services"

>
    <div class="container">
    <h1>
        <div class="row">
            
    <span class="material-icons">
        miscellaneous_services
        </span>
         Our Services
        </div></h1>
        <div class="row">
            <div class="col-md-4" >
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
                            location_on
                            </span>
                    </div>
                    <h4>
                        Geographic Information Systems
                    </h4>
                   
                </div>
            </div>
            <div class="col-md-4"
            >
                <div class="service-content">
                    <div class="contain-icon">
                    <i class="fas fa-stream"></i>
                    </div>
                    <h4>
                        Custom Software Development
                    </h4>
                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
cloud
</span>
                    </div>
                    <h4>
                        Web/Application Development 
                    </h4>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" >
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
polymer
</span>
                    </div>
                    <h4>
                    Quality Assurance
                    </h4>
                   
                </div>
            </div>
            <div class="col-md-4"
            >
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
web
</span>
                    </div>
                    <h4>
                    Web based Community Solutions.
                    </h4>
                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
timeline
</span>
                    </div>
                    <h4>
                    Client Server Applications
                    </h4>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" >
                <div class="service-content">
                    <div class="contain-icon">
                    <span class="material-icons">
open_with
</span>
                    </div>
                    <h4>
                    Enterprise Application Integration
                    </h4>
                   
                </div>
            </div>
        </div>
    </div>
</section>
    </div>
@endsection