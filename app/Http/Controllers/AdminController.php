<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Str;
use App\Projects;
use App\Users;
class AdminController extends Controller
{
    public function login(){
        return view('admin.login');
    }
    public function checkLogin(){
        
        request()->validate([
            'username' => 'required',
            'password' => 'required'
        ],[
            'username.required' => 'Enter username',
            'password.required' => 'Enter password'
        ]);
        $credentials = [
            'name' => request()->get('username'),
            'password' => request()->get('password')
        ];
        if(Auth::attempt($credentials)){
            return view('admin.dashoard');
        }
        return redirect()->back()->with('msg','Incorrect Credentials');
    }

    public function dashboard(){
        return view('admin.dashoard');
        
    }

    public function uploadProject(){
        return view('admin.uploadProject');
    }

    public function saveProject(){
        $file = request()->file('project');
        // return request()->all();
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'project' => 'required|dimensions:max_width>300,max_height>200'
        ],[
            'title.required' => 'Title is required', 
            'description.required' => 'Description is required', 
            'project.required' => 'Project Photo is required', 
            'project.dimensions' => 'Project photo width must be greater than 300 and height greater 200', 
        ]);  

        //Display File Extension
        $extention = $file->getClientOriginalExtension();
        echo '<br>';
        
     
        $project_id =  Str::random(20);
        $project_name =  Str::random(20);
        $project_name = $project_name .'.'. $extention;
        //Move Uploaded File
         $destinationPath = public_path().'/projects';
         
        
         request()->request->add(['photo_path' => $project_name]);
         request()->request->add(['uploaded_by' => 'User']);
         request()->request->add(['project_id' => $project_id]);
 
        $file->move($destinationPath,$project_name);
        Projects::create(request()->except('_token', 'project'));
        return redirect()->back()->with('msg', 'Upload Successful.');
    }

    public function allProject(){
        
        return view('admin.allProject')->with('projects', Projects::all());
    }

    public function deleteProject($id){
        unlink(public_path().'/'.Projects::where('id', $id)->value("photo_path"));        
        Projects::where('id', $id)->delete();
        return redirect()->back()->with('msg', 'Project Deleted Successfully..');
        
    }

    public function submitProject($id){
        Projects::where('id', $id)->update(['title'=> request()->get('title'), 'description' =>  request()->get('description')]);
        return redirect()->back()->with('msg', 'Project Edit Completed.');
        
    }

    public function editProject($id){
                
        return view('admin.editProject')->with('projects', Projects::where('id', $id)->get());
    }

    public function viewProject($id){
                
        return view('admin.viewProject')->with('projects', Projects::where('id', $id)->get()) ;
    }

    public function changePasword(){
        return view('admin.changePassword');
    }

    public function updatePasword(){

        $value = Users::where('name', Auth::user()->name)->value(password);
        $hashedvalue =  Hash::make(request()->get('password'));
        if(Hash::check($value, $hashedvalue))
            return "good";
            else
            return "bad";
        // request()->validate([
        //     'oldPassword' => 'required',
        //     'newPassword' => 'required|same:cPassword',
        //     'cPassword' => 'required|dimensions:max_width>300,max_height>200'
        // ],[
        //     'oldPassword.required' => 'Field is Required',
        //     'newPassword.required' => 'Field is Required',
        //     'cPassword.required' => 'Field is Required',
        //     'cPassword.same' => 'Password Field must match',
        // ]);

        // User::where('id', $id)->update(['password'=> Hash::make(request()->get('newPassword'))]);
        // return redirect()->back()->with('msg', 'Password Changed.');



    }
}
