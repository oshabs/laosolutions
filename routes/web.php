<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.index');
});

Route::get('/contact-us', function () {
    return view('site.contact');
});
Route::get('/about-us', function () {
    return view('site.about');
});
Route::get('/services', function () {
    return view('site.services');
});
Route::get('/all-projects', function () {
    return view('site.allProjects');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard', "AdminController@dashboard" );
    Route::get('signin', "AdminController@login" );
    Route::post('check-login-details', "AdminController@checkLogin" );
    Route::get('upload-project', "AdminController@uploadProject" );
    Route::post('upload-project', "AdminController@saveProject" );
    Route::get('all-project', "AdminController@allProject" );
    Route::get('project/delete/{i}', "AdminController@deleteProject" );
    Route::get('project/edit/{i}', "AdminController@editProject" );
    Route::get('view/project/{i}', "AdminController@viewProject" );
    Route::get('change-password', "AdminController@changePasword" );
    Route::post('change-password', "AdminController@updatePasword" );
    Route::post('edit/{i}', "AdminController@submitProject" );
});
